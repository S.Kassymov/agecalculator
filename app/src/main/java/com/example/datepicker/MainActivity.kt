package com.example.datepicker

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import org.w3c.dom.Text
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter.ofPattern
import java.util.*
import kotlin.time.milliseconds

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btn_click_me = findViewById(R.id.btnDatePicker) as Button

        btn_click_me.setOnClickListener { view ->
            clickDatePicker(view)
            Toast.makeText(this, "You clicked me", Toast.LENGTH_LONG).show()
        }
    }


    fun clickDatePicker(view: View) {
        val myCalendar = Calendar.getInstance()
        val year = myCalendar.get(Calendar.YEAR)
        val month = myCalendar.get(Calendar.MONTH)
        val day = myCalendar.get(Calendar.DAY_OF_MONTH)

         DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            val selectedDate = findViewById(R.id.selectedDate) as TextView
            val inMinutes = findViewById(R.id.inMinutes) as TextView

            val StringDate = "$dayOfMonth/${month+1}/$year"
            val formatter = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)

            val todayDate = formatter.parse(formatter.format(System.currentTimeMillis()))
            val choosenDate = formatter.parse(StringDate)

            val age = (todayDate.time - choosenDate.time) / 60000

            selectedDate.setText(StringDate)
            inMinutes.setText(age.toString())

            Toast.makeText(this, "Choosen year is " + year, Toast.LENGTH_LONG).show()
        }, year, month, day).show()
    }
}

